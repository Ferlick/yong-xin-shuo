<?php
namespace app\show\Controller;
use \think\Controller;
class Index extends Controller{
    public function index($coding=''){
        if($coding==''){
            $this->error('error');
        }
        if(!preg_match('/(?=.*?[a-zA-Z])(?=.*?[0-9])[a-zA-Z0-9]/',$coding)){
            $this->error('error');
        }
        $coding_len = strlen($coding);
        if($coding_len==6){ //品码进入
            $this->_product($coding);
        }else if($coding_len!=7){ //长度不是7位，非物码进入
            $this->error('coding length error');
        }
        if(isset($_GET['upload'])){ //上传成功回调处理
            header('Access-Control-Allow-Origin: *');
            header('Access-Control-Allow-Credentials:true');
            $filepath = getUpload($extend);
            if(!$filepath){
                echo 'error';
                exit;
            }
            echo $filepath;
            exit;
        }
        $Card = model('Card');
        $req = $Card->getInfo($coding,'id,template_id,product_id,owner_phone,is_content,end_time');
        if(!$req){
            $this->error('二维码不存在');
        }
        if(!$req['is_content']){ //还没写入内容
            if($req['end_time']<time()){ //写入时间已过期
                $this->error('二维码使用时间已过期，不可写入内容');
            }
            $this->_write($req['id'],$coding,$req['product_id'],$req['owner_phone'],$Card);
            exit;
        }
        //开始读取内容
        $row = $Card->getContent($req['id']);
        if(!$row){
            $this->error('二维码内容不存在或已过期');
        }
        if($row['look_way']==1){ //密码查看
            $this->_password($req['id'],$row['look_pwd_phone']);
            session('read_check_password',null);
        }else if($row['look_way']==2){ //短信验证码查看
            if(!isset($_GET['key'])){
                $this->_smscaptcha($row['look_pwd_phone']);
                session('read_check_phone',null);
            }else if(!session('read_check_phone') || hash('md5',$req['id'].'.'.$coding)!=$_GET['key']){
                $this->error('二维码内容不存在或已过期');
            }
        }
        if($req['product_id'] && $row['aging_status']==1){ //从物码生成，并且查看后立即失效
            $Card->setProductCardFailure($req['id']); //设置失效
        }
        //更新查看次数 +1
        $Card->setContentHits($row['id']);
        //获取模板代码
        $tpl_code = $Card->getTplCode($req['template_id']);
        $this->assign('content',json_decode($row['content'],true));
        return $this->fetch('/tpl/'.$tpl_code);
    }
    private function _write($card_id,$coding,$product_id,$_owner_phone,$Card){ //写入内容
        $owner_phone = session('product.'.$coding);
        if(!$owner_phone){
            $owner_phone = $this->_smscaptcha();
        }
        if($_owner_phone && $_owner_phone!=$owner_phone){
            $this->error('无权限操作');
        }
        if($_POST){
            $look_way = input('post.look_way','','trim'); //内容查看方式
            $look_pwd_phone = input('post.look_pwd_phone','','trim'); //查看内容的密码或手机号码
            $aging_status = input('post.aging_status','','trim'); //内容时效
            $content_text = htmlentities(strip_tags(input('post.content_text','','trim'))); //文本内容
            $content_image = input('post.content_image','','trim'); //图片内容  多张图片逗号隔开 ","
            $content_audio = input('post.content_audio','','trim'); //音频内容
            $content_video = input('post.content_video','','trim'); //视频内容
            if($look_way!='1' && $look_way!='2'){
                $this->error('内容查看方式选择错误',[
                    'focus' => 'look_way'
                ]);
            }
            if($look_way=='1' && !$product_id){ //密码查看
                if(!$look_pwd_phone){
                    $this->error('查看内容的密码不能为空',[
                        'focus' => 'password'
                    ]);
                }
                $look_way = 1;
                $look_pwd_phone = hash('md5',$card_id.'.'.$look_pwd_phone);
            }else{ //手机号验证查看
                if(!$look_pwd_phone || !check_account_format($look_pwd_phone)){
                    $this->error('查看内容的手机号码输入错误',[
                        'focus' => 'phone'
                    ]);
                }
                $look_way = 2;
            }
            if($aging_status!='1' && $aging_status!='2'){
                $this->error('内容时效选择错误',[
                    'focus' => 'aging_status'
                ]);
            }
            if(!$content_text && !$content_image && !$content_audio && !$content_video){
                $this->error('内容不能全为空');
            }
            $_content_image = [];
            if($content_image){
                $content_image = explode(',',$content_image);
                foreach($content_image as $i=>$val){
                    if($i>4){ //只能处理5张图片 大于5张则忽略  这里的4是数组索引号
                        break;
                    }
                    $_content_image[] = releaseFile($val);
                }
            }
            $content = [];
            $content['text'] = $content_text;
            $content['image'] = $_content_image;
            $content['audio'] = releaseFile($content_audio);
            $content['video'] = releaseFile($content_video);
            $req = $Card->setContent($owner_phone,[
                'card_id' => $card_id,
                'look_way' => $look_way,
                'look_pwd_phone' => $look_pwd_phone,
                'aging_status' => $aging_status,
                'content' => json_encode($content,JSON_UNESCAPED_UNICODE)
            ],$product_id);
            if(!$req){
                $this->error('内容写入失败');
            }
            session('write_check_phone',null);
            session('product.'.$coding,null);
            $this->success('内容写入成功');
        }else{
            $file = '/temp_image/'.date('Ym/d/',time()).'{random}{.suffix}';
            $image = setUpload($file,$coding);
            $file = '/temp_video/'.date('Ym/d/',time()).'{random}{.suffix}';
            $video = setUpload($file,$coding);
            $this->assign('image',$image);
            $this->assign('video',$video);
            $this->assign('is_product',$product_id);
            echo $this->fetch('/write');
        }
        exit;
    }
    private function _password($card_id,$pwd){ //输入密码查看内容
        if(session('read_check_password')){
            return true;
        }
        if($_POST){
            $password = input('post.password','','trim');
            if(!$password){
                $this->error('密码输入错误',[
                    'focus' => 'password',
                ]);
            }
            if(hash('md5',$card_id.'.'.$password)!=$pwd){
                $this->error('密码输入错误',[
                    'focus' => 'password'
                ]);
            }
            session('read_check_password',1);
            $this->success('密码验证成功');
        }else{
            echo $this->fetch('/password');
        }
        exit;
    }
    private function _smscaptcha($phone=''){ //短信验证码校验
        if($phone){ //品码物码读取时
            $sskey = 'read_check_phone';
            $_phone = $phone;
        }else{ //品码物码写入时
            $sskey = 'write_check_phone';
            $_phone = input('post.phone','','trim');
        }
        if(session($sskey)){ //手机校验已通过
            return session($sskey);
        }
        if($_POST){
            $_sendSMS = input('post.sendSMS','','trim');
            $_captcha = input('post.captcha','','trim');
            $_safecode = input('post.safecode','','trim');
            if(!$_phone || !check_account_format($_phone)){
                $this->error('手机号码输入错误',[
                    'focus' => 'phone'
                ]);
            }
            if(!$_safecode || ($_safecode!=config('general_code') && $_safecode!=session('safecode'))){
                $this->success('安全码输入错误');
            }
            //发送短信验证码
            if($_sendSMS=='1'){
                if(!captcha($_phone)){
                    $this->error('短信验证码发送失败，请稍候再试');
                }
                $this->success('短信验证码发送成功');
                exit;
            }
            if($_captcha!=config('general_code')){
                $captcha = session('captcha');
                if($captcha){
                    if(time()>$captcha['timeout']||$captcha['phone']!=$_phone){
                        session('captcha',null);
                        $this->error('短信验证码已过期，请重新获取',[
                            'focus' => 'captcha'
                        ]);
                    }
                    if($_captcha!=$captcha['number']){
                        $this->error('短信验证码输入错误',[
                            'focus' => 'captcha',
                        ]);
                    }
                    session('captcha',null);
                }else{
                    $this->error('请先获取短信验证码',[
                        'focus' => 'captcha',
                    ]);
                }
            }
            session($sskey,$_phone);
            $this->success('短信验证码校验成功');
        }else{
            echo $this->fetch('/smscaptcha');
        }
        exit;
    }
    private function _product($coding){ //品码
        $Card = model('Card');
        $req = $Card->getProductInfo($coding,'id,merchant_id,template_id');
        if(!$req){
            $this->error('二维码不存在');
        }
        if(isset($_GET['identity']) && ($_GET['identity']=='give' || $_GET['identity']=='take')){
            $funname = '_product_'.$_GET['identity'];
            $this->$funname($req,$Card);
        }else{
            echo $this->fetch('/product');
        }
        exit;
    }
    private function _product_give($product,$Card){ //送礼
        $phone = $this->_smscaptcha();
        $coding = $Card->setProductCard($product,$phone);
        if($coding===0){ //商户不存在
            $this->error('二维码商户不存在');
        }else if($coding===-1){ //商户物码可使用量已用完
            $this->error('二维码可使用量已用完，不可写入内容');
        }
        session('write_check_phone',null);
        session('product.'.$coding,$phone);
        header('Location: /'.$coding); //开始跳转
        exit;
    }
    private function _product_take($product,$Card){ //收礼
        $_phone = input('post.phone','','trim');
        $phone = $this->_smscaptcha($_phone);
        $card_id = $Card->getProductCardId($phone);
        $card_id_len = count($card_id);
        if(!$card_id_len){
            $this->error('没有相关数据');
        }else if($card_id_len==1){ //1条记录直接显示
            $req = $Card->getList($card_id[0]);
            $this->success('短信验证码校验成功',[
                'coding' => $req[0]['coding'],
                'key' => $req[0]['key'],
            ]);
        }else{ //多条记录  显示列表
            $card_id = implode(',',$card_id);
            $card_list = $Card->getList($card_id);
            $this->assign('card_list',$card_list);
            echo $this->fetch('/product_list');
        }
    }
    public function xk(){
        $time = time();
        for ($i=0; $i <200; $i++) { 
            $coding = random_str(7);
            $id = model('Card')->insertGetId([
                'coding' => $coding,
                'merchant_id' => 2,
                'template_id' => 1,
                'product_id' => 0,
                'owner_phone' => '',
                'owner_time' => 0,
                'time' => $time,
                'end_time' => $time+864000,
            ]);
            echo 'http://'.config('host').'/'.$coding.'<br />';
        }
    }
}