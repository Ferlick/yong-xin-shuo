<?php
namespace app\show\model;
use \think\Model;
class Card extends Model {
    //卡片码基本信息
    public function getInfo($coding,$field=''){
        return $this->field($field?$field:'id')->where([
            'coding' => $coding
        ])->find();
    }
    //根据物码ID 获取物码列表
    public function getList($card_id){
        if(is_numeric($card_id)){
            $where_val = $card_id;
        }else{
            $where_val = ['in',$card_id];
        }
        $list = $this->field('id,coding,template_id')->where([
            'id' => $where_val
        ])->order('id desc')->select();
        $list_arr = [];
        foreach($list as $row){
            $row['key'] = hash('md5',$row['id'].'.'.$row['coding']);
            $list_arr[] = $row;
        }
        return $list_arr;
    }
    //获取内容
    public function getContent($card_id){
        $req = $this->table('card_content')->field('id,look_way,look_pwd_phone,look_number,aging_status,content')->where([
            'card_id' => $card_id
        ])->find();
        if(!$req || ($req['aging_status']==1 && $req['look_number']>0)){
            return false;
        }
        return $req;
    }
    //更新查看次数 +1
    public function setContentHits($id){
        $this->table('card_content')->where([
            'id' => $id
        ])->setInc('look_number');
    }
    //获取模板代码
    public function getTplCode($tpl_id){
        return $this->table('card_template')->where([
            'id' => $tpl_id
        ])->value('tpl_code');
    }
    //写入内容
    public function setContent($owner_phone,$data,$product_id){
        $id = $this->table('card_content')->insertGetId($data);
        if(!$id){
            return false;
        }
        $req = $this->save([
            'owner_phone' => $owner_phone,
            'owner_time' => time(),
            'is_content' => 1
        ],[
            'id' => $data['card_id']
        ]);
        if(!$req){ //内容写入后更新拥有者信息失败
            $this->table('card_content')->delete($id); //删除写入的内容
            return false;
        }
        if($product_id){ //从品码生成时 需建立收礼人与物码关联
            $this->table('product_card')->insertGetId([
                'card_id' => $data['card_id'],
                'phone' => $data['look_pwd_phone'],
                'product_id' => $product_id,
            ]);
        }
        return true;
    }
    //品码基本信息
    public function getProductInfo($coding,$field=''){
        return $this->table('product')->field($field?$field:'id')->where([
            'coding' => $coding
        ])->find();
    }
    //通过品码生成物码
    public function setProductCard($product,$phone){
        $coding = random_str(7);
        $time = time();
        $id = $this->insertGetId([
            'coding' => $coding,
            'merchant_id' => $product['merchant_id'],
            'template_id' => $product['template_id'],
            'product_id' => $product['id'],
            'owner_phone' => $phone,
            'owner_time' => $time,
            'time' => $time,
            'end_time' => $time+864000,
        ]);
        if(!$id){
            return $this->setProductCard($product,$phone);
        }
        //判断品码所属商户是否还有物码可用
        $req = $this->table('merchant')->field('card_number,use_number')->where([
            'id' => $product['merchant_id']
        ])->find();
        if(!$req){
            $this->delete($id);
            return 0; //商户不存在
        }
        if($req['card_number']<=$req['use_number']){
            $this->delete($id);
            return -1; //物码可用量已用完
        }
        //减掉商户物码开通量
        $req = $this->table('merchant_card_log')->field('id,card_number,end_time,use_number')->where([
            'merchant_id' => $product['merchant_id'],
            'end_time' => ['>',$time]
        ])->order('end_time asc,id asc')->select();
        $merchant_card_log_id = 0;
        $merchant_card_log_end_time = '';
        foreach($req as $row){
            if($row['card_number']>$row['use_number']){
                $merchant_card_log_id = $row['id'];
                $merchant_card_log_end_time = $row['end_time'];
                break;
            }
        }
        if(!$merchant_card_log_id){
            $this->delete($id);
            return -1; //物码可用量已用完
        }
        $this->where('id',$id)->setField('end_time',$merchant_card_log_end_time); //更新物码写入到期时间
        $this->table('merchant_card_log')->where([
            'id' => $merchant_card_log_id
        ])->setInc('use_number');
        $this->table('merchant')->where([
            'id' => $product['merchant_id']
        ])->setInc('use_number');
        return $coding;
    }
    //通过品码生成物码，查看满足条件时设置失效
    public function setProductCardFailure($card_id){
        $this->table('product_card')->where('card_id',$card_id)->setField('is_failure',1);
    }
    //通过品码生成物码，根据收礼者手机号码查找物码
    public function getProductCardId($phone){
        return $this->table('product_card')->where([
            'phone' => $phone,
            'is_failure' => 0
        ])->column('card_id');
    }
}