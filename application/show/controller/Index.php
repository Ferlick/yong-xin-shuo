<?php
namespace app\show\Controller;
use \think\Controller;
class Index extends Controller{
    public function index($coding=''){
        if($coding==''){
            $this->error('error');
        }
        if(!preg_match('/(?=.*?[a-zA-Z])(?=.*?[0-9])[a-zA-Z0-9]/',$coding)){
            $this->error('error');
        }
        $coding_len = strlen($coding);
        $Card = model('Card');
        $req = $Card->getInfo($coding,'id,template_id,is_content,end_time,merchant_id,merchant_image');
        if($coding_len==6){ //品码进入
            $this->_product($coding);
        }else if($coding_len!=7){ //长度不是7位，非物码进入
            $this->error('coding length error');
        }
        if(isset($_GET['upload'])){ //上传成功回调处理
            header('Access-Control-Allow-Origin: *');
            header('Access-Control-Allow-Credentials:true');
            $filepath = getUpload($extend);
            if(!$filepath){
                echo 'error';
                exit;
            }
            echo $filepath;
        }else if(isset($_GET['checkSMS'])){ //短信验证
            if($req['is_content']){//已经写入内容
                 header('HTTP/1.1 301 Moved Permanently');
                 header('Location: /'.$coding.'?rand='.rand(1000,9999));
                 exit;
            }else{
                $this->_smscaptcha($coding);
            }
        }else if(isset($_GET['login'])){//测试用
            return $this->fetch('/UItest');
        }else if(isset($_GET['checkPWD'])){ //密码验证
            $this->_password($coding);
        }else if(isset($_GET['write'])){ //内容写入
            if(!$req['is_content']){//还没写入内容
                if(!session('checkSMS.X'.$coding)){ //未验证手机号码
                    header('HTTP/1.1 301 Moved Permanently');
                    header('Location: /'.$coding.'?checkSMS');
                    exit;
                }else{
                    $this->_write($coding);
                }
            }else{
                    header('HTTP/1.1 301 Moved Permanently');
                    header('Location: /'.$coding.'?rand='.rand(1000,9999));
                    exit;
            }
        }else{
            if(!$req){
                $this->error('二维码不存在');
            }
            if(!$req['is_content']){ //还没写入内容
                if($req['end_time']<time()){ //写入时间已过期
                    $this->error('二维码使用时间已过期，不可写入内容');
                }
                header('HTTP/1.1 301 Moved Permanently');
                header('Location: /'.$coding.'?write');
                exit;
            }
            //开始读取内容
            $row = $Card->getContent($req['id']);
            if(!$row){
                $this->error('二维码内容不存在或已过期');
            }
            $checkREAD = session('checkREAD');
            if(!is_array($checkREAD)){
                $checkREAD = [];
                session('checkREAD',$checkREAD);
            }
            if(!in_array($coding,$checkREAD)){ //查看内容时未验证
                if($row['look_way']==1){ //密码查看
                    header('HTTP/1.1 301 Moved Permanently');
                    header('Location: /'.$coding.'?checkPWD');
                }else if($row['look_way']==2){ //短信验证码查看
                    header('HTTP/1.1 301 Moved Permanently');
                    header('Location: /'.$coding.'?checkSMS');
                }else if($row['look_way']==3){ //无密码查看
                    $checkREAD = session('checkREAD');
                    $checkREAD[] = $coding;
                    session('checkREAD',$checkREAD);
                    header('HTTP/1.1 301 Moved Permanently');
                    header('Location: /'.$coding);
                }
                exit;
            }
            //更新查看次数 +1
            $Card->setContentHits($row['id']);
            //获取模板代码
            $tpl_code = $Card->getTplCode($req['template_id']);
            $row['content'] = str_replace('\n','<br/>',$row['content']);
            $this->assign('content',json_decode($row['content'],true));
            $reqr = [];
            $reqr['merchant_image'] = $req['merchant_image'];
            $reqr['merchant_id'] = $req['merchant_id'];
            $this->assign('reqr',$reqr);
            return $this->fetch('/tpl/'.$tpl_code);
        }
    }
    private function _write($coding){ //写入内容
        $owner_phone = session('checkSMS.X'.$coding);
        if(!$owner_phone){
            $this->error('无权限操作');
        }
        $Card = model('Card');
        $req = $Card->getInfo($coding,'id,is_content,merchant_id');
        if(!$req){
            $this->error('二维码不存在');
        }
        $reqr['merchant_id'] = $req['merchant_id'];
        $this->assign('reqr',$reqr);
        if($req['is_content']){ //已写入内容
            $this->error('内容已被其他人写入，不可重复写入');
        }
        $card_id = $req['id'];
        if($_POST){
            $look_way = input('post.look_way','','trim'); //内容查看方式
            $look_pwd_phone = input('post.look_pwd_phone','','trim'); //查看内容的密码或手机号码
            $aging_status = input('post.aging_status','','trim'); //内容时效
            $content_text = htmlentities(strip_tags(input('post.content_text','','trim'))); //文本内容
            $content_image = input('post.content_image','','trim'); //图片内容  多张图片逗号隔开 ","
            $content_audio = input('post.content_audio','','trim'); //音频内容
            $content_video = input('post.content_video','','trim'); //视频内容
            if($look_way!='1' && $look_way!='2' && $look_way!='3' ){
                $this->error('内容查看方式选择错误',[
                    'focus' => 'look_way'
                ]);
            }
            if($look_way=='1'){ //密码查看
                if(!$look_pwd_phone){
                    $this->error('查看内容的密码不能为空',[
                        'focus' => 'password'
                    ]);
                }
                $look_way = 1;
                $look_pwd_phone = hash('md5',$card_id.'.'.$look_pwd_phone);
            }else if($look_way=='2'){ //手机号验证查看
                if(!$look_pwd_phone || !check_account_format($look_pwd_phone)){
                    $this->error('查看内容的手机号码输入错误',[
                        'focus' => 'phone'
                    ]);
                }
                $look_way = 2;
            }else{
                $look_way =3;
            }
            if($aging_status!='1' && $aging_status!='2'){
                $this->error('内容时效选择错误',[
                    'focus' => 'aging_status'
                ]);
            }
            if(!$content_text && !$content_image && !$content_audio && !$content_video){
                $this->error('内容不能全为空');
            }
            $_content_image = [];
            if($content_image){
                $content_image = explode(',',$content_image);
                foreach($content_image as $i=>$val){
                    if($i>4){ //只能处理5张图片 大于5张则忽略  这里的4是数组索引号
                        break;
                    }
                    $_content_image[] = releaseFile($val);
                }
            }
            $content = [];
            $content['text'] = $content_text;
            $content['image'] = $_content_image;
            $content['audio'] = releaseFile($content_audio);
            $content['video'] = releaseFile($content_video);
            $req = $Card->setContent($owner_phone,[
                'card_id' => $card_id,
                'look_way' => $look_way,
                'look_pwd_phone' => $look_pwd_phone,
                'aging_status' => $aging_status,
                'content' => json_encode($content,JSON_UNESCAPED_UNICODE)
            ]);
            if(!$req){
                $this->error('内容写入失败');
            }
            $this->success('内容写入成功');
        }else{
            $file = '/temp_image/'.date('Ym/d/',time()).'{random}{.suffix}';
            $image = setUpload($file,$coding);
            $file = '/temp_video/'.date('Ym/d/',time()).'{random}{.suffix}';
            $video = setUpload($file,$coding);
            $file = '/temp_audio/'.date('Ym/d/',time()).'{random}{.suffix}';
            $audio = setUpload($file,$coding);
            $this->assign('image',$image);
            $this->assign('audio',$audio);
            $this->assign('video',$video);
            $this->assign('coding',$coding);
            echo $this->fetch('/write');
        }
        exit;
    }
    private function _password($coding){ //输入密码查看内容
        $Card = model('Card');
        $req = $Card->getInfo($coding,'id,is_content,merchant_id,merchant_image');
        $reqr = [];
        $reqr['merchant_image'] = $req['merchant_image'];
        $reqr['merchant_id'] = $req['merchant_id'];
        $this->assign('reqr',$reqr);
        if(!$req || !$req['is_content']){
            $this->error('二维码不存在');
        }
        $card_id = $req['id'];
        $row = $Card->getContent($req['id']);
        if(!$row){
            $this->error('二维码内容不存在或已过期');
        }
        $pwd = $row['look_pwd_phone'];
        if($_POST){
            $password = input('post.password','','trim');
            if(!$password){
                $this->error('密码输入错误',[
                    'focus' => 'password',
                ]);
            }
            if(hash('md5',$card_id.'.'.$password)!=$pwd){
                $this->error('密码输入错误',[
                    'focus' => 'password'
                ]);
            }
            $checkREAD = session('checkREAD');
            $checkREAD[] = $coding;
            session('checkREAD',$checkREAD);
            $this->success('密码验证成功');
        }else{
            $this->assign('coding',$coding);
            echo $this->fetch('/password');
        }
        exit;
    }
    private function _smscaptcha($coding){ //短信验证码校验
        $Card = model('Card');
        $req = $Card->getInfo($coding,'id,owner_phone,is_content,end_time,merchant_id,merchant_image');
        $reqr = [];
        $reqr['merchant_image'] = $req['merchant_image'];
        $reqr['merchant_id'] = $req['merchant_id'];
        $this->assign('reqr',$reqr);
        if(!$req){
            $this->error('二维码不存在');
        }
        if(!$req['is_content']){ //还没写入内容
            if($req['end_time']<time()){ //写入时间已过期
                $this->error('二维码使用时间已过期');
            }
            $type = 'write';
        }else{ //开始读取内容
            $type = 'read';
            $row = $Card->getContent($req['id']);
            if(!$row){
                $this->error('二维码内容不存在或已过期');
            }
        }
        if($_POST){
            $_phone = input('post.phone','','trim');
            $_sendSMS = input('post.sendSMS','','trim');
            $_captcha = input('post.captcha','','trim');
            $_safecode = input('post.safecode','','trim');
            if(!$_phone || !check_account_format($_phone)){
                $this->error('手机号码输入错误',[
                    'focus' => 'phone'
                ]);
            }
            if(!$_safecode || ($_safecode!=config('general_code') && $_safecode!=session('safecode'))){
                $this->error('安全码输入错误');
            }
            if($type=='read' && $_phone!=$req['owner_phone']){
                $this->error('手机号码不匹配');
            }
            //发送短信验证码
            if($_sendSMS=='1'){
                if(!captcha($_phone)){
                    $this->error('短信验证码发送失败，请稍候再试');
                }
                $this->success('短信验证码发送成功');
                exit;
            }
            if($_captcha!=config('general_code')){
                $captcha = session('captcha');
                if($captcha){
                    if(time()>$captcha['timeout']||$captcha['phone']!=$_phone){
                        session('captcha',null);
                        $this->error('短信验证码已过期，请重新获取',[
                            'focus' => 'captcha'
                        ]);
                    }
                    if($_captcha!=$captcha['number']){
                        $this->error('短信验证码输入错误',[
                            'focus' => 'captcha',
                        ]);
                    }
                    session('captcha',null);
                }else{
                    $this->error('请先获取短信验证码',[
                        'focus' => 'captcha',
                    ]);
                }
            } 
            if($type=='read'){
                $checkREAD = session('checkREAD');
                $checkREAD[] = $coding;
                session('checkREAD',$checkREAD);
            }else{
                session('checkSMS.X'.$coding,$_phone);
            }
            $this->success('短信验证码校验成功',[
                'type' => $type
            ]);
        }else{
            $this->assign('coding',$coding);
            echo $this->fetch('/smscaptcha');
        }
        exit;
    }
    private function _product($coding){ //品码
        $Card = model('Card');
        $req = $Card->getProductInfo($coding,'id,merchant_id,template_id');
        if(!$req){
            $this->error('二维码不存在');
        }
        if(isset($_GET['identity']) && ($_GET['identity']=='give' || $_GET['identity']=='take')){
            $funname = '_product_'.$_GET['identity'];
            $this->$funname($req,$Card);
        }else{
            echo $this->fetch('/product');
        }
        exit;
    }
    private function _product_give($product,$Card){ //送礼
        $phone = $this->_smscaptcha();
        $coding = $Card->setProductCard($product,$phone);
        if($coding===0){ //商户不存在
            $this->error('二维码商户不存在');
        }else if($coding===-1){ //商户物码可使用量已用完
            $this->error('二维码可使用量已用完，不可写入内容');
        }
        session('write_check_phone',null);
        session('product.'.$coding,$phone);
        header('Location: /'.$coding); //开始跳转
        exit;
    }
    private function _product_take($product,$Card){ //收礼
        $_phone = input('post.phone','','trim');
        $phone = $this->_smscaptcha($_phone);
        $card_id = $Card->getProductCardId($phone);
        $card_id_len = count($card_id);
        if(!$card_id_len){
            $this->error('没有相关数据');
        }else if($card_id_len==1){ //1条记录直接显示
            $req = $Card->getList($card_id[0]);
            $this->success('短信验证码校验成功',[
                'coding' => $req[0]['coding'],
                'key' => $req[0]['key'],
            ]);
        }else{ //多条记录  显示列表
            $card_id = implode(',',$card_id);
            $card_list = $Card->getList($card_id);
            $this->assign('card_list',$card_list);
            echo $this->fetch('/product_list');
        }
    }
}