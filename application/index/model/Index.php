<?php
namespace app\index\model;
use \think\Model;
class Index extends Model {
    //获取商家信息
    public function getMerchant($account){
        $req = $this->table('merchant')->field('id,account,password,name,card_number,use_number,card_generate')->where([
            'account' => $account
        ])->find();
        return $req;
    }
    //获取所属商家码的基本信息
    public function getMerchantCard($merchant_id,$field=""){
        $req = $this->table('card')->field($field?$field:'id,coding,template_id,owner_phone,owner_time,is_content,merchant_image')->where([
            'merchant_id' => $merchant_id
        ])->find();
        return $req;
    }
    //获取当前商户卡片使用情况
    public function getMerchantCardUserInfo($merchant_id,$is_content='',$field=''){
        $req = $this->table('card')->field($field?$field:'id,coding,template_id,owner_phone,owner_time,merchant_image')->where([
            'merchant_id' => $merchant_id,
            'is_content' => $is_content,
        ])->select();
        return $req;
    }
    //获取当前商户卡片使用量
    public function getMerchantCardUserNumber($where,$field=''){
        $req = $this->table('card')->field($field?$field:'id,coding,owner_phone')->where($where)->count();
        return $req;
    }
    //更新商户状态
    public function updateMerchant($merchant_id,$content){
        $req = $this->table('merchant')->where('id',$merchant_id)->update([
            'use_number' => $content['use_number'],
            'password' => $content['password'],
            'name'=> $content['name'],
            'card_number' => $content['card_number'],
            'card_generate' => $content['card_generate']
        ]);
        return $req;
    }
        //写入批次
    public function setBatch($merchant_id,$data){
        $batch_id = $this->table('merchant_batch')->insertGetId($data);
        if(!$batch_id){
            return false;
        }
        return $batch_id;
    }
}