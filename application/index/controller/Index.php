<?php
namespace app\index\Controller;
use \think\Controller;
session_start();
class Index extends Controller
{
    public function index()
    {
        $index = model('Index'); 
        if(isset($_GET['member'])){ 
            return $this->fetch('/main');
        }else if(isset($_GET['login'])){
                if($_POST){
                $index = model('Index');
                $row = $index->getMerchant($_POST['account']);
                if(!$row){
                    $this->error('用户名错误',[
                        'focus' => 'account'
                    ]);
                }
                $password = md5($_POST['password']);
                if($row['password'] != $password){
                    $this->error('密码错误',[
                        'focus' => 'password'
                    ]);
                }else if(!$_POST['captcha'] || ($_POST['captcha']!=config('general_code') && $_POST['captcha']!=session('safecode'))){
                    $this->error('验证码错误',[
                        'focus' => 'captcha'
                    ]);
                }else{
                    $_SESSION['id'] = $row['id'];
                    $_SESSION['account'] = $row['account'];
                    header('HTTP/1.1 301 Moved Permanently');
                    header('Location: /?admin');
                    exit;
                }
            }else{
        	    return $this->fetch('/login');
            }
        }else if(isset($_GET['admin'])){
            if(!$_SESSION['id']){
                header('HTTP/1.1 301 Moved Permanently');
                header('Location: /?login');
                exit;
            }else{
                $row = $index->getMerchant($_SESSION['account']);

                $list=[];
                $list['name'] = $row['name'];
                $list['password'] = $row['password'];
                $list['card_number'] = $row['card_number'];

                $Card_use = $index->getMerchantCardUserNumber(array('merchant_id'=>$row['id'],'is_content'=>1));//获取已使用的码量
                $list['use_number'] = $Card_use;
                $Card_generate = $index->getMerchantCardUserNumber(array('merchant_id'=>$row['id']));
                $list['card_generate'] = $Card_generate;
                $Card_save = $index->updateMerchant($row['id'],$list);//整合数据
                
                $this->assign('list',$list);
                return $this->fetch('/admin/index');
            }
        }else if(isset($_GET['qrcode'])){
            if(!$_SESSION['id']){
                header('HTTP/1.1 301 Moved Permanently');
                header('Location: /?login');
                exit;
            }else{
                if($_POST){
                    $this->qrcode();
                }else{
                    $this->error('添加失败，请咨询管理员！！');
                }
            }
        }else{
            return $this->fetch('/index');
        }
    }
    
    //批量查看环节
    public function main(){
      $index = model('Index');
      $row = $index->getMerchant($_SESSION['account']);

      $list=[];
      $list['name'] = $row['name'];
      $list['password'] = $row['password'];
      $list['use_number'] = $row['use_number'];
      $list['card_number'] = $row['card_number'];
      $list['card_generate'] = $row['card_generate'];
      $this->assign('list',$list);
      return  $this->fetch('/admin/main');
    }

    //自动生码环节
    public function qrcode(){
        if(!$_SESSION['id']){
            header('HTTP/1.1 301 Moved Permanently');
            header('Location: /?login');
            exit;
        }else{
            $index = model('Index');
            $row = $index->getMerchant($_SESSION['account']);

            if($_POST){
                $row = $index->getMerchant($_SESSION['account']);
                if($row['card_number'] <= $row['card_generate']){
                    $this->error('您的码量已用光，请联系官方管理员申请加码');
                    return false;
                }
                //$img = $this->saveimage(input('post.merchant_image','','trim'));

                $qrcode_num = $row['card_generate'] + $_POST['card_generate'];
                if($qrcode_num > $row['card_number']){
                    $this->error('您的码量不足，请再确认后输入要生成的码量！！');
                    return false;
                }else{
                    $time = time();
                    $req = $index->setBatch($_SESSION['id'],[
                        'merchant_id' => $_SESSION['id'],
                        'code_num' => $_POST['card_generate'],
                        'merchant_image' => '',//merchant_image
                        'merchant_image_url' => '',//merchant_image_url
                        'code_num' => $_POST['card_generate'],
                        'tpl_code' => '1',//tpl_code
                        'batch_name' => $_POST['batch_name']
                    ]);
                    for ($i=0; $i <$_POST['card_generate']; $i++) { 
                        $coding = random_str(7);
                        $id = $index->table('card')->insertGetId([
                            'coding' => $coding,
                            'batch_id' => $req,
                            'merchant_id' => $_SESSION['id'], //商户ID
                            'template_id' => '1',//tpl_code
                            'product_id' => 0,
                            'owner_phone' => '',
                            'owner_time' => 0,
                            'time' => $time,
                            'end_time' => $time+31536000,
                        ]);
                        echo 'http://'.config('host').'/'.$coding.'<br />';
                    }
                    
                }
            }else{
                $list=[];
                $list['name'] = $row['name'];
                $list['password'] = $row['password'];
                $list['use_number'] = $row['use_number'];
                $list['card_number'] = $row['card_number'];
                $this->assign('list',$list);
                echo $this->fetch('/admin/qrcode');
            }
        }
    }

    public function logout()
    {
        session(null,'admin');

        $this->redirect('index/login');
    }

}
