<?php
function check_account_format($phone){
  return !(!$phone || !preg_match('/^((13[0-9])|(17[0-9])|(15[^4,\D])|(18[0-9]))\d{8}$/',$phone));
}
function loginpwd($account,$loginpwd){
  return hash('md5',$account.'.'.$loginpwd);
}
function random_str($len){
  $str = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
  $str = str_shuffle($str);
  $coding = substr($str,0,$len);
  if(!preg_match('/(?=.*?[a-zA-Z])(?=.*?[0-9])[a-zA-Z0-9]/',$coding)){
    return random_str($len);
  }
  return $coding;
}
//发送短信验证码
function captcha($phone){
  $time = time();
  $captcha = session('captcha');
  if($captcha){
    if($time<$captcha['sendtime']){
        return false;
    }
  }
  $captcha = rand(1000,9999);
  $req = sendSMS($phone,'captcha',array(
    'code' => $captcha
  ));
  if(!$req){
    return false;
  }
  session('captcha',array(
    'sendtime' => $time+60,
    'timeout' => $time+540,
    'number' => $captcha,
    'phone' => $phone
  ));
  return true;
}
/*
$Mobilephone = 接收短信的手机号码
$type = 内容类型
    captcha 验证码
$data = 附加数据  数组
*/
function sendSMS($Mobilephone,$type,$data=''){
  if(config('sms_status')!='1'){ //关闭短信推送
    return true;
  }
  $smstpl = config('sms_tpl');
  if(!$type || !$smstpl[$type]){
    return false;
  }
  $smstext = $smstpl[$type];
  if(is_array($data)){
    foreach($data as $key=>$val) {
      $smstext = str_replace('#'.$key.'#',$val,$smstext);
    }
  }
  $url = 'https://sms.yunpian.com/v1/sms/send.json';
  $post_data = array(
    'apikey' => '57d6ff71a2d47598ed935f9388e8de5b',
    'mobile' => $Mobilephone,
    'text' => $smstext
  );
  $header = array();
  $header[] = 'Accept:application/json';  
  $header[] = 'charset: utf-8';  
  $header[] = 'Content-Type: application/x-www-form-urlencoded';  

  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, $url);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
  curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
  curl_setopt($ch, CURLOPT_TIMEOUT,10);
  curl_setopt($ch, CURLOPT_POST, 1);
  curl_setopt($ch, CURLOPT_HTTPHEADER, $header); 
  curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post_data));
  $req = curl_exec($ch);
  if(curl_error($ch)){ //请求出错  包括超时
    curl_close($ch);
    return false;
  }
  curl_close($ch);
  $req = json_decode($req,true);
  if($req['code']!='0'){
    return false;
  }
  return true;
}
//又拍云上传
function setUpload($filename,$return_path){
  $upyun = config('upyun');
  $options = array();
  $options['bucket'] = $upyun['bucket']; //空间名
  $options['expiration'] = time()+6000; //授权过期时间
  $options['save-key'] = $filename;
  $options['allow-file-type'] = 'jpg,jpeg,gif,png,mp4,avi,mov,3gp,mpg,f4v,m4v';
  $options['return-url'] = $_SERVER['REQUEST_SCHEME'].'://'.config('host').'/'.$return_path.'?upload'; //页面跳转回调地址
  $policy = base64_encode(json_encode($options));
  $signature = hash('md5',$policy.'&'.$upyun['secret']);
  return array(
      'host' => $upyun['host'],
      'bucket' => $options['bucket'],
      'policy' => $policy,
      'signature' => $signature
  );
}
//校验结果同时获取文件路径
function getUpload(&$extend=''){
  $upyun = config('upyun');
  $pram = array();
  $pram[] = $_GET['code'];
  $pram[] = $_GET['message'];
  $pram[] = $_GET['url'];
  $pram[] = $_GET['time'];
  $pram[] = $upyun['secret'];
  if(hash('md5',implode('&',$pram))!=$_GET['sign']){
      return false;
  }
  if($extend!==''){
    $fia = explode('.',basename($_GET['url']));
    $fia = explode('-',$fia[0]);
    if(count($fia)>1){
      $extend = $fia[0];
    }
  }
  return $upyun['domain'].$_GET['url'];
}
//将临时文件发布成正式文件
function releaseFile($file_path){
  $_file_path = '';
  if(!$file_path){
    return $_file_path;
  }
  $newx = explode('temp_',$file_path);
  if(isset($newx[1])){
      $temp_file = '/temp_'.$newx[1];
      $offi_file = '/'.$newx[1];
      if(!isset($GLOBALS['_FTP'])){
        $GLOBALS['_FTP'] = new \show\ftp();
        $GLOBALS['_FTP_START'] = $GLOBALS['_FTP']->start(config('upyun.ftp'));
      }
      if($GLOBALS['_FTP_START']){
          if($GLOBALS['_FTP']->move($temp_file,$offi_file)){
              $_file_path = str_replace('temp_','',$file_path);
          }
      }
  }
  return $_file_path;
}
//删除又拍云文件
function delUpload($fileurl){
  /*
  $c_upyun = config('upyun');
  $upyun = new \Common\Util\upyun($c_upyun['bucket'],$c_upyun['ftp']['username'],$c_upyun['ftp']['password']);
  $upyun->delete(str_replace($c_upyun['domain'],'',$fileurl),array(
    'x-upyun-async' => true //异步删除
  ));
  */
}
