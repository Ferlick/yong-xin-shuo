<?php if (!defined('THINK_PATH')) exit(); /*a:1:{s:70:"/home/wwwroot/gcard.cc/public/../application/show/view/smscaptcha.html";i:1495873268;}*/ ?>
<!DOCTYPE html>
<?php if($reqr['merchant_id'] == '3'): ?>
<html lang="zh-cmn-Hans">
<head>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0" />
<title>用心说</title>
<link rel="stylesheet" href="//res.wx.qq.com/open/libs/weui/1.1.1/weui.min.css" />
<link rel="stylesheet" href="/static/css/app.css" />
<style>
    .login-img{
        margin: 20px 0;
    }
    .login-img img{
        width: 100%;
        height: auto;
    }
</style>
</head>
<body ontouchstart>
<div style="display:none;" id="_alert">
    <div class="weui-mask"></div>
    <div class="weui-dialog">
        <div class="weui-dialog__hd"><strong class="weui-dialog__title">提示</strong></div>
        <div class="weui-dialog__bd"></div>
        <div class="weui-dialog__ft">
            <a href="javascript:;" class="weui-dialog__btn weui-dialog__btn_primary" id="_oks">确定</a>
        </div>
    </div>
</div>
<div class="header">
    <div class="weui-cells__title">验证手机号码</div>
</div>
<form id="submit-form" action="/<?php echo $coding; ?>?checkSMS" method="POST">
    <div class="weui-cells weui-cells_form">
        <div class="weui-cell weui-cell_vcode">
            <div class="weui-cell__hd"><label class="weui-label">手机号</label></div>
            <div class="weui-cell__bd">
                <input class="weui-input" type="tel" name="phone" placeholder="请输入你的手机号" />
            </div>
        </div>
        <div class="weui-cell weui-cell_vcode">
            <div class="weui-cell__hd"><label class="weui-label">安全码</label></div>
            <div class="weui-cell__bd">
                <input class="weui-input" type="tel" name="safecode" placeholder="请输入安全码" />
            </div>
            <div class="weui-cell__ft"><img class="weui-vcode-img" src="/show/verify" /></div>
        </div>
        <div class="weui-cell weui-cell_vcode">
            <div class="weui-cell__bd">
                <input class="weui-input" type="number" name="captcha" placeholder="手机短信验证码" />
            </div>
            <div class="weui-cell__ft">
                <span id="smscountdown" class="weui-vcode-btn" style="display:none;color:#ccc">60秒后重新获取</span>
                <a href="javascript:;" id="getCaptcha" class="weui-vcode-btn">获取验证码</a>
            </div>
        </div>
    </div>
    <div class="container" style="margin-top:1rem">
        <button type="submit" class="weui-btn weui-btn_primary">确定</button>
    </div>
</form>
<?php if($reqr['merchant_image'] == '1'): ?>
<div class="login-img">
    <img src="/static/image/wap/bannar.jpg">
</div>
<?php endif; if($reqr['merchant_image'] == '3'): ?>
<div class="login-img">
    <img src="/static/image/wap/bannar3.jpg">
</div>
<?php endif; ?>
<script src="//cdn.bootcss.com/jquery/2.1.3/jquery.min.js"></script>
<script src="//res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
<script src="//res.wx.qq.com/open/libs/weuijs/1.0.0/weui.min.js"></script>
<script type="text/javascript">
var alert = function(str,callback){
    alert.callback = callback;
    $("#_alert").css('display','block').find(".weui-dialog__bd").html(str);
};
$("#_oks").click(function(){
    if(typeof alert.callback=='function'){
        alert.callback();
    }
    $("#_alert").css('display','none');
});
$(function(){
    var sms_send = 0,is_x,data = {},url;
    $("#getCaptcha").bind("click",function(){
        sms_send = 1;
        $("#submit-form").trigger("submit");
        if(!is_x){
            return false;
        }
        data.sendSMS = 1;
        $.post(url,data,function(req){
            if(req.code==1){
                (function(time){
                    var displayTime = setInterval(function(){
                        if(time<=1){
                            $("#getCaptcha").show();
                            $("#smscountdown").hide().html("60秒后重新获取");
                            clearInterval(displayTime);
                        }
                        --time;
                        $("#smscountdown").html(time+"秒后重新获取");
                    },1000);
                })(60);
                $("#getCaptcha").hide();
                $("#smscountdown").show();
            }else{ //发送失败
                alert(req.msg);
            }
        },'json');
    });
    $("#submit-form").bind("submit",function(){
        var _this = this;
        data.phone = $.trim(_this.phone.value);
        data.safecode = $.trim(_this.safecode.value);
        data.captcha = $.trim(_this.captcha.value);
        url = $(_this).attr("action");
        is_x = 1;
        if(!/^((13[0-9])|(17[0-9])|(15[^4,\D])|(18[0-9]))\d{8}$/.test(data.phone)){
            alert("手机号码输入错误",function(){
                _this.phone.focus();
            });
            is_x = 0;
            return false;
        }
        is_x = 1;
        if(!/^[0-9a-zA-Z]{4}$/.test(data.safecode)){
            alert("安全码输入错误",function(){
                _this.safecode.focus();
            });
            is_x = 0;
            return false;
        }
        if(sms_send==1){
            sms_send = 0;
            return false;
        }
        if(!/^[0-9]{4}$/.test(data.captcha)){
            alert("短信验证码输入错误",function(){
                _this.captcha.focus();
            });
            return false;
        }
        data.sendSMS = null;
        delete data.sendSMS;
        $.post(url,data,function(req){
            if(req.code==1){
                window.location.href = '/<?php echo $coding; ?>'+(req.data.type=='write'?'?'+req.data.type:'');
            }else{
                alert(req.msg,function(){
                    req.data.focus && _this[req.data.focus].focus();
                });
            }
        },'json');
        return false;
    });
});
</script>
</body>
</html>
<?php endif; if($reqr['merchant_id'] == '2' || $reqr['merchant_id'] == '4'): ?>
<html lang="zh-cmn-Hans">
<head>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0" />
<title>用心说</title>
<link rel="stylesheet" href="//res.wx.qq.com/open/libs/weui/1.1.1/weui.min.css" />
<link rel="stylesheet" href="/static/css/app.css" />
<style>
    *{
        padding: 0;
        margin: 0;
    }
    #index{
        width: 100%;
        height: 100%;
        position: absolute;
        left:0;
        top:0;
    }
    .indexBottom{
        width: 100%;
        height: 100%;
    }
    .top{
        height:35%;
        width: 100%;
        background:#ff6199;
        padding-bottom: 3rem;
    }
    .top .logo{
        width:80%;
        height:auto;
        margin: auto;
    }
    .login{
        margin:auto;
        width: 90%;
        overflow: hidden;
        background: #fff;
        margin-top: 40%;
        border-radius:20px;
        padding-top: 1rem;
        box-shadow: 0 0 20px #aaa;
    }
    #submit-form .weui-cell__bd .weui-input{
        padding-left: 3rem;
        height: 2rem;
    }
    .weui-cell{
        border-bottom: 1px solid #d9d9d9;
    }
    .weui-cell:before{
        border:0;
    }
    #submit-form .i-phone i{
       background: url('/static/icons/phone.png') no-repeat;
    }
    #submit-form .i-safecode i{
       background: url('/static/icons/safecode.png') no-repeat;
    }
    #submit-form .i-captcha i{
       background: url('/static/icons/captcha.png') no-repeat;
    }
    #submit-form .i-captcha i,#submit-form .i-safecode i,#submit-form .i-phone i{
       width: 22px;
       height: 22px;
       position: absolute;
       top:50%;
       margin-top:-11px;
       margin-left:9px;
    }
    .weui-cells:after, .weui-cells:before {
        height:0;
        border:0;
    }
    button.weui-btn{
        margin-top: 1.5rem;
        width: 60%;
        border-radius: 1rem;
        background:#ff6199;
    }
    .weui-cell__bd{ 
        margin:1rem 0;
        border:0;
    }
</style>
</head>
<body>
<div style="display:none;" id="_alert">
<div class="weui-mask"></div>
    <div class="weui-dialog">
        <div class="weui-dialog__hd"><strong class="weui-dialog__title">提示</strong></div>
        <div class="weui-dialog__bd"></div>
        <div class="weui-dialog__ft">
            <a href="javascript:;" class="weui-dialog__btn weui-dialog__btn_primary" id="_oks">确定</a>
        </div>
    </div>
</div>
<div id="index">
    <div class="indexBottom">
        <div class="top">
            <div class="logo">
                <img src="/static/image/wap/logo-pass.jpg" style="width:100%;height:100%;">
            </div>
        </div>
        <div class="bottom"></div>
    </div>
</div>
<div style="position:absolute;width:100%;height:100%;">
    <div class="login">
        <form id="submit-form" action="<?php echo $coding; ?>?checkSMS" method="POST">
            <div class="weui-cells weui-cells_form">
                <div class="weui-cell weui-cell_vcode i-phone">
                    <div class="weui-cell__bd">
                        <i></i>
                        <input class="weui-input" type="tel" name="phone" placeholder="请输入手机号" />
                    </div>
                </div>
                <div class="weui-cell weui-cell_vcode i-safecode">
                    <div class="weui-cell__bd" >
                        <i></i>
                        <input class="weui-input" type="tel" name="safecode" placeholder="请输入安全码" />
                    </div>
                    <div class="weui-cell__ft"><img class="weui-vcode-img" style="height:3rem;" src="/show/verify" /></div>
                </div>
                <div class="weui-cell weui-cell_vcode i-captcha">
                    <div class="weui-cell__bd">
                        <i></i>
                        <input class="weui-input" type="number" name="captcha" placeholder="手机短信验证码" />
                    </div>
                    <div class="weui-cell__ft">
                        <span id="smscountdown" class="weui-vcode-btn" style="display:none;color:#ccc">60秒后重新获取</span>
                        <a href="javascript:;" id="getCaptcha" class="weui-vcode-btn">获取验证码</a>
                    </div>
                </div>
            </div>
            <div class="container" style="margin-top:1rem;margin-bottom:2rem;">
                <button type="submit" class="weui-btn weui-btn_primary">确定</button>
            </div>
        </form>
    </div>
</div>
</body>
<script src="//cdn.bootcss.com/jquery/2.1.3/jquery.min.js"></script>
<script src="//res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
<script src="//res.wx.qq.com/open/libs/weuijs/1.0.0/weui.min.js"></script>
<script type="text/javascript">
var alert = function(str,callback){
    alert.callback = callback;
    $("#_alert").css('display','block').find(".weui-dialog__bd").html(str);
};
$("#_oks").click(function(){
    if(typeof alert.callback=='function'){
        alert.callback();
    }
    $("#_alert").css('display','none');
});
$(function(){
    var sms_send = 0,is_x,data = {},url;
    $("#getCaptcha").bind("click",function(){
        sms_send = 1;
        $("#submit-form").trigger("submit");
        if(!is_x){
            return false;
        }
        data.sendSMS = 1;
        $.post(url,data,function(req){
            if(req.code==1){
                (function(time){
                    var displayTime = setInterval(function(){
                        if(time<=1){
                            $("#getCaptcha").show();
                            $("#smscountdown").hide().html("60秒后重新获取");
                            clearInterval(displayTime);
                        }
                        --time;
                        $("#smscountdown").html(time+"秒后重新获取");
                    },1000);
                })(60);
                $("#getCaptcha").hide();
                $("#smscountdown").show();
            }else{ //发送失败
                alert(req.msg);
            }
        },'json');
    });
    $("#submit-form").bind("submit",function(){
        var _this = this;
        data.phone = $.trim(_this.phone.value);
        data.safecode = $.trim(_this.safecode.value);
        data.captcha = $.trim(_this.captcha.value);
        url = $(_this).attr("action");
        is_x = 1;
        if(!/^((13[0-9])|(17[0-9])|(15[^4,\D])|(18[0-9]))\d{8}$/.test(data.phone)){
            alert("手机号码输入错误",function(){
                _this.phone.focus();
            });
            is_x = 0;
            return false;
        }
        is_x = 1;
        if(!/^[0-9a-zA-Z]{4}$/.test(data.safecode)){
            alert("安全码输入错误",function(){
                _this.safecode.focus();
            });
            is_x = 0;
            return false;
        }
        if(sms_send==1){
            sms_send = 0;
            return false;
        }
        if(!/^[0-9]{4}$/.test(data.captcha)){
            alert("短信验证码输入错误",function(){
                _this.captcha.focus();
            });
            return false;
        }
        data.sendSMS = null;
        delete data.sendSMS;
        $.post(url,data,function(req){
            if(req.code==1){
                window.location.href = '/<?php echo $coding; ?>'+(req.data.type=='write'?'?'+req.data.type:'');
            }else{
                alert(req.msg,function(){
                    req.data.focus && _this[req.data.focus].focus();
                });
            }
        },'json');
        return false;
    });
});
</script>
</html>
<?php endif; ?>