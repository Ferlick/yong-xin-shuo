<?php if (!defined('THINK_PATH')) exit(); /*a:1:{s:66:"/home/wwwroot/gcard.cc/public/../application/index/view/login.html";i:1495692574;}*/ ?>
﻿
<!DOCTYPE html>
<html lang="en" class="no-js">

<head>

<meta charset="utf-8">
<title>用心说后台登录</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">

<!-- CSS -->
<link rel="shortcut icon"href="/static/icons/logo.ico"> 
<link rel="stylesheet" href="/static/css/supersized.css">
<link rel="stylesheet" href="/static/css/login.css">
<link href="/static/css/bootstrap.min.css" rel="stylesheet">
<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
	<script src="js/html5.js"></script>
<![endif]-->
</head>
<script src="/static/js/jquery-1.8.2.min.js"></script>
<script type="text/javascript" src="/static/js/login.js"></script>
<script type="text/javascript" src="/static/js/jquery.form.js"></script>
<script type="text/javascript" src="/static/js/tooltips.js"></script>
<body>

<div class="page-container">
	<div class="main_box">
		<div class="login_box">
			<div class="login_logo">
				<img src="/static/image/logo.png" >
			</div>
			<div class="login_form">
				<form action="" id="login-form" method="post">
					<div class="form-group">
						<label for="j_username" class="t">用户名：</label> 
						<input id="account" name="account" type="text" class="form-control x319 in" autocomplete="off">
					</div>
					<div class="form-group">
						<label for="j_password" class="t">密　码：</label> 
						<input id="password"name="password" type="password" 
						class="password form-control x319 in">
					</div>
					<div class="form-group">
						<label for="j_captcha" class="t">验证码：</label>
						 <input id="captcha" name="captcha" type="text" class="form-control x164 in" > 
						<img id="captcha_img" alt="点击更换" title="点击更换" src="/show/verify" onclick="this.src=this.src+'?a=1'" class="m">
					</div>
<!-- 					<div class="form-group">
						<label class="t"></label>
						<label for="j_remember" class="m">
						<input id="j_remember" type="checkbox" value="true">&nbsp;记住登陆账号!</label>
					</div> -->
                    <!-- {{csrf_field()}} -->
					<div class="form-group space">
						<label class="t"></label>　　　
						<button type="submit"  id="submit" 
						class="btn btn-primary btn-lg">&nbsp;登&nbsp;录&nbsp </button>
						<input type="reset" value="&nbsp;重&nbsp;置&nbsp;" class="btn btn-default btn-lg">
					</div>
				</form>
			</div>
		</div>
		<div class="bottom">Copyright &copy; 2014 - 2015 <a href="#">用心说</a></div>
	</div>
</div>

<!-- Javascript -->

<script src="/static/js/supersized.3.2.7.min.js"></script>
<script src="/static/js/supersized-init.js"></script>
<script src="/static/js/scripts.js"></script>
<script type="text/javascript">
	account=password=captcha=false;
	$('input[name="account"]').blur(function(){
		account=$(this).val();
		if(!account){
			account=false;
			show_err_msg('用户名还没填呢！');	
			// $('#account').focus();
		}else{
			account=true;
			$('input[name="password"]').blur(function(){
				password=$(this).val();
				if (!password) {
					password=false;
					show_err_msg('密码还没填呢！');	
					// $('#password').focus();			
				}else{
					password=true;
					$('input[name="captcha"]').blur(function(){
							captcha=$(this).val();
							if (!captcha) {
								captcha=false;
								show_err_msg('验证码不能为空！');	
								// $('#captcha').focus();			
							}else{
								captcha=true;
							}
						});
				}
			});
		}
	});

	$(function(){
		$("#login-form").bind("submit",function(){
				var data = {};
				data.account = $('#account').val();
				data.password = $('#password').val();
				data.captcha = $('#captcha').val();
				console.log('2');
				$.post(url,data,function(req){
					console.log('1');
					if(req == 'account'){
						alert('用户名出错！！');
					}else if(req == 'password'){
						alert('密码出错！！');
					}else if(req == 'captcha'){
						alert('验证码出错！！');
					}else{}
				},'json');
		})
	});
	/**
	$.post(url,data,function(req){
            if(req.code==1){
                clearTimeout(timeout);
                $("#loadingToast").hide();
                $("#submit-form").css('display','none');
                $("#main-index").css('display','block');
            }else{
                alert(req.msg,function(){
                    req.data.focus && _this[req.data.focus].focus();
                });
            }
        },'json');
	**/
</script>
</body>
</html>