<?php if (!defined('THINK_PATH')) exit(); /*a:2:{s:73:"/home/wwwroot/gcard.cc/public/../application/index/view/admin/qrcode.html";i:1495871492;s:80:"/home/wwwroot/gcard.cc/public/../application/index/view/admin/layout/layout.html";i:1496207581;}*/ ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>后台首页</title>
    <link rel="shortcut icon"href="/static/icons/logo.ico"> 
    <!-- Bootstrap -->
    <link href="/home/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="/home/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="/home/vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- bootstrap-daterangepicker -->
    <link href="/home/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="/home/build/css/custom.min.css" rel="stylesheet">
    
  </head>
  
  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="?admin" class="site_title"><i class="fa fa-paw"></i> <span>首页</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="/home/production/images/img.jpg" alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Welcome,</span>
                <h2><?php echo $list['name']; ?></h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>General</h3>
                <ul class="nav side-menu">
                  <li><a href="/?admin"><i class="fa fa-home"></i> 概况 </a>
                  </li>
                  
                </ul>
              </div>
              <div class="menu_section">
                <h3>操作</h3>
                <ul class="nav side-menu">
                  <li><a href="<?php echo url('main'); ?>" target="mainFrame"><i class="fa fa-bug"></i>二维码生成</a>
                    <!--<ul class="nav child_menu">
                      <li><a href="e_commerce.html">E-commerce</a></li>
                      <li><a href="projects.html">Projects</a></li>
                      <li><a href="project_detail.html">Project Detail</a></li>
                      <li><a href="contacts.html">Contacts</a></li>
                      <li><a href="profile.html">Profile</a></li>
                    </ul>-->
                  </li>
                  <li><a><i class="fa fa-sitemap"></i> Multilevel Menu <span class="fa fa-chevron-down"></span></a>
                    <!--<ul class="nav child_menu">
                        <li><a href="#level1_1">Level One</a>
                        <li><a>Level One<span class="fa fa-chevron-down"></span></a>
                          <ul class="nav child_menu">
                            <li class="sub_menu"><a href="level2.html">Level Two</a>
                            </li>
                            <li><a href="#level2_1">Level Two</a>
                            </li>
                            <li><a href="#level2_2">Level Two</a>
                            </li>
                          </ul>
                        </li>
                        <li><a href="#level1_2">Level One</a>
                        </li>
                    </ul>-->
                  </li>                  
                  <li><a href="javascript:void(0)"><i class="fa fa-laptop"></i> Landing Page <span class="label label-success pull-right">Coming Soon</span></a></li>
                </ul>
              </div>

            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
              <a data-toggle="tooltip" data-placement="top" title="Settings">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Lock">
                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Logout" href="{}">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
              </a>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img src="/home/production/images/img.jpg" alt=""><?php echo $list['name']; ?>
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                    <li><a href="javascript:;"> Profile</a></li>
                    <li>
                      <a href="javascript:;">
                        <span class="badge bg-red pull-right">50%</span>
                        <span>Settings</span>
                      </a>
                    </li>
                    <li><a href="javascript:;">Help</a></li>
                    <li><a href="<?php echo url('index/logout'); ?>"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                  </ul>
                </li>

                <li role="presentation" class="dropdown">
                  <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
                    <i class="fa fa-envelope-o"></i>
                    <span class="badge bg-green">6</span>
                  </a>
                  <ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">
                    <li>
                      <a>
                        <span class="image"><img src="images/img.jpg" alt="Profile Image" /></span>
                        <span>
                          <span>John Smith</span>
                          <span class="time">3 mins ago</span>
                        </span>
                        <span class="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
                      </a>
                    </li>
                    <li>
                      <a>
                        <span class="image"><img src="images/img.jpg" alt="Profile Image" /></span>
                        <span>
                          <span>John Smith</span>
                          <span class="time">3 mins ago</span>
                        </span>
                        <span class="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
                      </a>
                    </li>
                    <li>
                      <a>
                        <span class="image"><img src="images/img.jpg" alt="Profile Image" /></span>
                        <span>
                          <span>John Smith</span>
                          <span class="time">3 mins ago</span>
                        </span>
                        <span class="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
                      </a>
                    </li>
                    <li>
                      <a>
                        <span class="image"><img src="images/img.jpg" alt="Profile Image" /></span>
                        <span>
                          <span>John Smith</span>
                          <span class="time">3 mins ago</span>
                        </span>
                        <span class="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
                      </a>
                    </li>
                    <li>
                      <div class="text-center">
                        <a>
                          <strong>See All Alerts</strong>
                          <i class="fa fa-angle-right"></i>
                        </a>
                      </div>
                    </li>
                  </ul>
                </li>
              </ul>
            </nav>
          </div>
        </div>
        <div class="right_col" role="main">
        <div>
            <form action="" id="qr-code" method="post">
					<div class="form-group">
						<label for="j_generate" class="t">生码数量：</label> 
						<input id="card_generate" name="card_generate" type="text" class="form-control x319 in" autocomplete="off">
					</div>
					<div class="form-group">
						<label for="j_batch_name" class="t">批次名称：</label> 
						<input id="batch_name"name="batch_name" type="text" class="form-control x319 in">
					</div>
					<div class="form-group">
						<label for="j_tpl_code" class="t">模板号：</label>
						 <select>
                             <option>1</option>
                             <option>2</option>
                             <option>3</option>
                         </select>
					</div>
                    <div class="form-group">
                        <label for="j_merchant_image">上传图片：</label>
                        <input type="file" name="merchant_image" id="merchant_image" onchange="ok()"> 
                        <input type="hidden" name="MAX_FILE_SIZE" value="30000" />  
                    </div>
                    <div class="form-group">
                        <label for="j_merchant_image_url">请输入图片链接：</label>
                        <input id="merchant_image_url"name="merchant_image_url" type="text" class="form-control x319 in">
                    </div>
					<div class="form-group space">
						<label class="t"></label>　　　
						<button type="submit"  id="submit" 
						class="btn btn-primary btn-lg">&nbsp;登&nbsp;录&nbsp </button>
						<input type="reset" value="&nbsp;重&nbsp;置&nbsp;" class="btn btn-default btn-lg">
					</div>
				</form>
        </div>
        </div>
      </div>
    </div>
    <!-- jQuery -->
    <script src="/home/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="/home/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="/home/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="/home/vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="/home/vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- jQuery Sparklines -->
    <script src="/home/vendors/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
    <!-- Flot -->
    <script src="/home/vendors/Flot/jquery.flot.js"></script>
    <script src="/home/vendors/Flot/jquery.flot.pie.js"></script>
    <script src="/home/vendors/Flot/jquery.flot.time.js"></script>
    <script src="/home/vendors/Flot/jquery.flot.stack.js"></script>
    <script src="/home/vendors/Flot/jquery.flot.resize.js"></script>
    <!-- Flot plugins -->
    <script src="/home/vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="/home/vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="/home/vendors/flot.curvedlines/curvedLines.js"></script>
    <!-- DateJS -->
    <script src="/home/vendors/DateJS/build/date.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="/home/vendors/moment/min/moment.min.js"></script>
    <script src="/home/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
    
    <!-- Custom Theme Scripts -->
    <script src="/home/build/js/custom.min.js"></script>
    <script language="javascript">  
        function $(id){  
            return document.getElementById(id);  
            }  
        function ok(){  
            $("logoimg").src = $("filename").value;  
        }
        $(function(){
		$("#login-form").bind("submit",function(){
            if($("#card_generate").val() != null && $("#batch_name").val() != null && $("#merchant_image").val() != null && $("#merchant_image_url").val() != null){
                $card_generate = $("#card_generate");
				var data = new FormData();
                data.append('card_generate',$("#card_generate").val())
				data.card_generate = $("#card_generate").val();
				data.batch_name = $("#batch_name").val();
				data.merchant_image = $("#merchant_image");
                data.merchant_image_url = $("#merchant_image_url").val();
                var url = '?qrcode';
				$.post(url,data,function(req){
					
				},'json');
            }
		})
	});
    </script> 
  </body>
</html>