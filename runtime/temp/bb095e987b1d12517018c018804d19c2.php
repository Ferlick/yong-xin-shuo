<?php if (!defined('THINK_PATH')) exit(); /*a:1:{s:65:"/home/wwwroot/gcard.cc/public/../application/show/view/write.html";i:1495519922;}*/ ?>
<!DOCTYPE html>
<html lang="zh-cmn-Hans">
<head>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0" />
<title>用心说</title>
<link rel="stylesheet" href="//res.wx.qq.com/open/libs/weui/1.1.1/weui.min.css" />
<link rel="stylesheet" href="/static/css/app.css" />
<link rel="stylesheet" href="/static/css/bootstrap.min.css">
<link rel="stylesheet" href="/static/css/highlight.css">
<link rel="stylesheet" href="/static/css/bootstrap-switch.css">
<link rel="stylesheet" href="/static/css/docs.min.css">
<link rel="stylesheet" href="/static/css/main.css">
<style>
    #main-index{
    padding: 10px 30px;
    width: 100%;
    height: 100%;
}
.write-end{
    text-align: center;
    padding-top:40%;
    padding-bottom:20%;
}
.write-end .circle{
    border: 5px solid #fff;
    border-radius: 100%;
    width: 10rem;
    height: 10rem;
    margin-left:auto;
    margin-right:auto;
}
.write-end .circle img{
    width: 5rem;
    height: 5rem;
    border-radius: 5rem;
}
.write-end p{
    font-size: 3rem;
}
.write-button{
    width: 60%;
    margin:auto;
    box-sizing: border-box;
}
.look_the_qrcode,.close_qrcode{
    width: 100%;
    height: 40px;
    border-radius: 10px;
    border:1px solid #ff6199;
}
.look_the_qrcode{
    color: #fff;
    background:#ff6199;
}
.close_qrcode{
    color: #ff6199;
    margin-top: 2rem;
    background:#fff;
}
footer{
    position: absolute;
    left:0;
    width: 100%;
    text-align: center;
    bottom: 0;
}
.weui-uploader__bd .weui-uploader__input-box:before{
    position: absolute;
    top: 50%;
    left: 50%;
    background: url('/static/icons/camera.png') no-repeat;
    background-size: 95%;
    width: 50px;
    height: 50px;
}
.weui-cell__bd p{
    margin:10px 0;
}
ol,ul{
    margin-bottom:0;
}
.weui-cells{
    margin-top:0;
}
#icvideo .weui-uploader__input-box:before{
    position: absolute;
    top: 50%;
    left: 50%;
    background: url('/static/icons/video.png') no-repeat;
    background-size: 95%;
    width: 50px;
    height: 50px;
}
.weui-uploader__input-box:after,.image-line .weui-cells:after,.weui-cells_form:before{
    width:0;
    height:0;
}
.video-line .weui-cells:before{
    height: 0;
    border:0;
}
button.weui-btn{
    margin-top: 10px;
    background: #ff6199;
    width: 60%;
}
.bootstrap-switch-id-switch-time .bootstrap-switch-container,.bootstrap-switch-id-switch-way .bootstrap-switch-container{
    background: #eeeeee;
}
</style>
</head>
<body ontouchstart>
<div style="display:none;" id="_alert">
    <div class="weui-mask"></div>
    <div class="weui-dialog">
        <div class="weui-dialog__hd"><strong class="weui-dialog__title">提示</strong></div>
        <div class="weui-dialog__bd"></div>
        <div class="weui-dialog__ft">
            <a href="javascript:;" class="weui-dialog__btn weui-dialog__btn_primary" id="_oks">确定</a>
        </div>
    </div>
</div>
<div id="loadingToast" style="display:none;">
    <div class="weui-mask_transparent"></div>
    <div class="weui-toast">
        <i class="weui-loading weui-icon_toast"></i>
        <p class="weui-toast__content"></p>
    </div>
</div>
<form id="submit-form" action="" method="POST" style="padding-top:2.5rem">
    <div id="step1">
        <div class="weui-cells weui-cells_form">
            <div class="weui-cell">
                <div class="weui-cell__bd">
                    <textarea class="weui-textarea" name="content_text" placeholder="说点什么.." rows="3"></textarea>
                </div>
            </div>
        </div>
        <div class="weui-gallery" id="gallery" style="background:rgba(0,0,0,0.7)">
            <span class="weui-gallery__img" id="galleryImg" style="margin:30px;"></span>
            <div class="weui-gallery__opr">
                <a href="javascript:" class="weui-gallery__del" id="galleryDel">
                    <i class="weui-icon-delete weui-icon_gallery-delete" style="font-size: 2rem;"></i>
                </a>
            </div>
        </div>
        <div class="weui-uploader image-line">
            <div class="weui-cells" style="margin-top:0;padding:.8rem 1rem .5rem 1rem">
                <div class="weui-uploader__bd">
                    <ul class="weui-uploader__files" id="uploaderFiles"></ul>
                    <div id="kaimage" class="weui-uploader__input-box" style="background:#d9d9d9;">
                        <input id="uploaderInput" class="weui-uploader__input" type="file" accept="image/*" />
                    </div>
                </div>
            </div>
        </div>
        <div class="weui-uploader video-line">
            <div class="weui-cells" id="icvideo" style="margin-top:0;padding:.8rem 1rem 1rem 1rem">
                <div class="weui-uploader__input-box" id="kavideo" style="float:none;background:#d9d9d9;">
                    <input id="uploaderVideo" class="weui-uploader__input" type="file" accept="video/*" />
                </div>
            </div>
        </div>
        <!--<div class="weui-uploader">
            <div class="weui-uploader__hd" style="padding:.5rem 1rem;">
                <p class="weui-uploader__title">音频</p>
                <span id="xreset" style="display:none">重置</span>
            </div>
            <div class="weui-cells" style="margin-top:0;padding:.8rem 1rem 1rem 1rem">
                <div class="weui-uploader__input-box" id="kaaudio" style="margin:0 auto;float:none">
                    <input id="uploaderAudio" class="weui-uploader__input" type="file" accept="audio/*;capture=microphone" />
                </div>
            </div>
        </div>-->
    </div>
    <!--<div style="display:none" id="step2">-->
        <div class="weui-cells weui-cells_radio">
            <label class="weui-cell weui-check__label" for="x13">
                <div class="weui-cell__bd">
                    <p>查看后立即失效</p>
                </div>
                <div class="weui-cell__ft">
                    <input type="checkbox"  id="switch-time" name="aging_status" value="2" />
                    <span class="weui-icon-checked"></span>
                </div>
            </label>
            <!--<label class="weui-cell weui-check__label" for="x14">
                <div class="weui-cell__bd">
                    <p>永久有效</p>
                </div>
                <div class="weui-cell__ft">
                    <input type="radio" name="aging_status" value="2" class="weui-check" id="x14" checked="checked" />
                    <span class="weui-icon-checked"></span>
                </div>
            </label>-->
        </div>
        <div class="weui-cells weui-cells_radio">
            <label class="weui-cell weui-check__label" style="display:none;" for="x12">
                <div class="weui-cell__bd">
                    <p>手机号码验证查看</p>
                </div>
                <div class="weui-cell__ft">
                    <input type="radio" name="look_way" value="2" class="weui-check" id="x12"/>
                    <span class="weui-icon-checked"></span>
                </div>
            </label>
            <label class="weui-cell weui-check__label" for="x11">
                <div class="weui-cell__bd">
                    <p>密码查看</p>
                </div>
                <div class="weui-cell__ft">
                    <input type="checkbox" id="switch-way" name="look_way" value="3" />
                    <span class="weui-icon-checked"></span>
                </div>
            </label>
            <!--<label class="weui-cell weui-check__label" for="x10">
                <div class="weui-cell__bd">
                    <p>无密码</p>
                </div>
                <div class="weui-cell__ft">
                    <input type="radio" class="weui-check" value="3" name="look_way" id="x10"  />
                    <span class="weui-icon-checked"></span>
                </div>
            </label>-->
            <div class="weui-cell weui-cell_vcode" id="look_way_1" style="display:none;">
                <div class="weui-cell__bd">
                    <input class="weui-input" type="password" name="password" placeholder="请输入查看密码" />
                </div>
            </div>
            <div class="weui-cell weui-cell_vcode" style="display:none"  id="look_way_2">
                <div class="weui-cell__bd">
                    <input class="weui-input" type="tel" name="phone" placeholder="请输入对方手机号码" />
                </div>
            </div>
        </div>
    <!---</div>-->
    <div class="container" style="margin:1rem 0">
        <button type="submit" class="weui-btn weui-btn_primary">确定</button>
    </div>
</form>
<div id="main-index" style="display:none;">
        <div class="write-end">
            <div class="circle"><img src="/static/image/wap/confim.png"></div>
            <p>保存成功<br/>欢迎使用</p>
        </div>
        <div class="write-button">
            <button class="look_the_qrcode" onclick="location.replace(location.href);">预览</button>
            <button class="close_qrcode" onclick="doclose();">完成</button>
            </div>
        <footer>Design by 用心说</footer>
</div>
<script src="/static/js/jquery-1.11.1.min.js"></script>
<script src="//res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
<script src="//res.wx.qq.com/open/libs/weuijs/1.0.0/weui.min.js"></script>
<script src="/static/js/bootstrap.min.js"></script>
<script src="/static/js/highlight.js"></script>
<script src="/static/js/bootstrap-switch@3.3.4"></script>
<script src="/static/js/main.js"></script>
<script type="text/javascript">
var alert = function(str,callback){
    alert.callback = callback;
    $("#_alert").css('display','block').find(".weui-dialog__bd").html(str);
};
$("#_oks").click(function(){
    if(typeof alert.callback=='function'){
        alert.callback();
    }
    $("#_alert").css('display','none');
});
$(function(){
    (function(){
        //图片
        var photo_num = 0;
        var tmpl = '<li class="weui-uploader__file" style="background-image:url(#url#)"><input type="hidden" class="imagex" value="#url#" /></li>',
            $gallery = $("#gallery"),
            $galleryImg = $("#galleryImg"),
            $uploaderInput = $("#uploaderInput"),
            $uploaderFiles = $("#uploaderFiles");
        $uploaderInput.on("change", function(e){
            if(!$uploaderInput[0].files[0]){
                return;
            }
            var formData = new FormData();
            formData.append('file', $uploaderInput[0].files[0]);
            formData.append('policy','<?php echo $image['policy']; ?>');
            formData.append('signature','<?php echo $image['signature']; ?>');
            $("#loadingToast").show().find("p").html("上传中...");
            $.ajax({
                url : '<?php echo $image['host']; ?>/<?php echo $image['bucket']; ?>',
                type : 'POST',
                cache : false,
                data : formData,
                processData : false,
                contentType : false
            }).done(function(req){
                $("#loadingToast").hide();
                $uploaderFiles.append($(tmpl.replace(/#url#/g, req)));
                $("#photo_num").html(++photo_num);
            }).fail(function() {
                $("#loadingToast").hide();
                alert("上传失败");
            });
        });
        var $this;
        $uploaderFiles.on("click", "li", function(){
            $this = $(this);
            $galleryImg.attr("style", this.getAttribute("style"));
            $galleryImg.css('margin','30px');
            $gallery.fadeIn(100);
        });
        $gallery.on("click", function(){
            $gallery.fadeOut(100);
        });
        $("#galleryDel").on("click", function(){
            $("#photo_num").html(--photo_num);
            $this.remove();
        });
        //视频
        var $uploaderVideo = $("#uploaderVideo");
        $uploaderVideo.on("change", function(e){
            if(!$uploaderVideo[0].files[0]){
                return;
            }
            var formData = new FormData();
            formData.append('file', $uploaderVideo[0].files[0]);
            formData.append('policy','<?php echo $video['policy']; ?>');
            formData.append('signature','<?php echo $video['signature']; ?>');
            $("#loadingToast").show().find("p").html("上传中...");
            $.ajax({
                url : '<?php echo $video['host']; ?>/<?php echo $video['bucket']; ?>',
                type : 'POST',
                cache : false,
                data : formData,
                processData : false,
                contentType : false
            }).done(function(req){
                $("#xreset").show();
                $("#loadingToast").hide();
                $("#kavideo").hide().after('<video src="'+req+'" id="videoxr" style=""controls="controls" width="100%" height="100%" preload="preload"><input type="hidden" class="videox" value="'+req+'" /></video>');
                $('#videoxr').css('background','url("/static/image/wap/video-background.jpg") no-repeat');
                $('#videoxr').css('background-size','cover');
            }).fail(function(){
                $("#loadingToast").hide();
                alert("上传失败");
            });
        });
        //音频
        var $uploaderAudio = $("#uploaderAudio");
        $uploaderAudio.on("change", function(e){
            if(!$uploaderAudio[0].files[0]){
                return;
            }
            var formData = new FormData();
            formData.append('file', $uploaderAudio[0].files[0]);
            formData.append('policy','<?php echo $audio['policy']; ?>');
            formData.append('signature','<?php echo $audio['signature']; ?>');
            $("#loadingToast").show().find("p").html("上传中...");
            $.ajax({
                url : '<?php echo $audio['host']; ?>/<?php echo $audio['bucket']; ?>',
                type : 'POST',
                cache : false,
                data : formData,
                processData : false,
                contentType : false
            }).done(function(req){
                $("#xreset").show();
                $("#loadingToast").hide();
                $("#kaaudio").hide().after('<audio src="'+req+'" controls="controls" width="100%" height="100%" preload="preload"><input type="hidden" class="videox" value="'+req+'" /></audio>');
            }).fail(function() {
                $("#loadingToast").hide();
                alert("上传失败");
            });
        });

        $("#xreset").click(function(){
            $("#videoxr").remove();
            $("#kavideo").show();
            $("#xreset").hide();
        });
    })();  
     $('#switch-time').on('switchChange.bootstrapSwitch', function (event,state) {  
           if(state ==true){
               this.value = '1';
               $('.bootstrap-switch-id-switch-time .bootstrap-switch-container').css('background','#ff6199');
           }else{
               this.value = '2';
               $('.bootstrap-switch-id-switch-time .bootstrap-switch-container').css('background','#eeeeee');
           }
     });  
     $('#switch-way').on('switchChange.bootstrapSwitch', function (event,state) {  
           if(state==true){
               this.value = '1';
               $("#look_way_1").show();
               $('.bootstrap-switch-id-switch-way .bootstrap-switch-container').css('background','#ff6199');
           }else{
               this.value = '3';
               $("#look_way_1").hide();
               $('.bootstrap-switch-id-switch-way .bootstrap-switch-container').css('background','#eeeeee');
           }
     });  
    $("#submit-form").bind("submit",function(){
        var _this = this;
        var url = $(_this).attr("action");
        var data = {};
        var change_textarea = $.trim(_this.content_text.value);
        data.content_text = change_textarea.replace('<br/>','\n');
        var imagex = [];
        $(".imagex").each(function(){
            imagex.push(this.value);
        });
        data.content_image = imagex.join(',');
        data.content_video = $(".videox").val();
        data.aging_status = _this.aging_status.value;
        data.look_way = $('#switch-way').val();
        if(!data.content_text && !data.content_image && !data.content_video){
            alert("内容不能全为空");
            return false;
        }
       
        if(data.look_way=='1'){ //密码查看
            data.look_pwd_phone = $.trim(_this.password.value);
            if(!data.look_pwd_phone){
                alert("查看密码不能为空",function(){
                    _this.password.focus();
                   
                });
                return false;
            }
        }else if(data.look_way=='2'){
            data.look_pwd_phone = $.trim(_this.phone.value);
            if(!/^((13[0-9])|(17[0-9])|(15[^4,\D])|(18[0-9]))\d{8}$/.test(data.look_pwd_phone)){
                alert("手机号码输入错误",function(){
                    _this.phone.focus();
                });
                return false;
            }
        }else{
            data.look_pwd_phone = '';
        }
        $("#loadingToast").show().find("p").html("上传中...");
        var timeout = setTimeout(function(){
                $("#loadingToast").hide();
                $("#submit-form").css('display','none');
                $("#main-index").css('display','block');
        },10000);
        $.post(url,data,function(req){
            if(req.code==1){
                clearTimeout(timeout);
                $("#loadingToast").hide();
                $("#submit-form").css('display','none');
                $("#main-index").css('display','block');
            }else{
                alert(req.msg,function(){
                    req.data.focus && _this[req.data.focus].focus();
                });
            }
        },'json');
        return false;
    });
});
</script>
    <script>
        function doclose(){
            WeixinJSBridge.call('closeWindow');
            var userAgent = navigator.userAgent;
            if (userAgent.indexOf("Firefox") != -1 || userAgent.indexOf("Chrome") !=-1) {
            window.location.href="about:blank";
            }else{
            window.opener = null;
            window.open("about:blank", "_self");
            window.close();
            WeixinJSBridge.invoke('closeWindow',{},function(res){
                //alert(res.err_msg);
            });
            }
        }
    </script>
</body>
</html>