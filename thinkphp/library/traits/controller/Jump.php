<?php

/**
 * 用法：
 * load_trait('controller/Jump');
 * class index
 * {
 *     use \traits\controller\Jump;
 *     public function index(){
 *         $this->error();
 *         $this->redirect();
 *     }
 * }
 */
namespace traits\controller;

use think\Config;
use think\exception\HttpResponseException;
use think\Request;
use think\Response;
use think\response\Redirect;
use think\Url;
use think\View as ViewTemplate;

trait Jump
{
    private $is_cross_domain_iframe = false;
    private $is_cross_domain_http_host = '';
    private $is_cross_domain_callback = '';
    protected function success($msg = '', $data = '', $url = null, $wait = 3, array $header = [])
    {
        $code = 1;
        if (is_numeric($msg)) {
            $code = $msg;
            $msg  = '';
        }
        $result = [
            'code' => $code,
            'msg'  => $msg,
            'data' => $data
        ];
        $this->cross_iframe($result);
        $type = $this->getResponseType();
        if ('html' == strtolower($type)) {
            if (is_null($url) && isset($_SERVER["HTTP_REFERER"])) {
                $url = $_SERVER["HTTP_REFERER"];
            } elseif ('' !== $url) {
                $url = (strpos($url, '://') || 0 === strpos($url, '/')) ? $url : Url::build($url);
            }
            $result['url'] = $url;
            $result['wait'] = $wait;
            $result = ViewTemplate::instance(Config::get('template'), Config::get('view_replace_str'))
                ->fetch(Config::get('dispatch_success_tmpl'), $result);
        }
        $response = Response::create($result, $type)->header($header);
        throw new HttpResponseException($response);
    }
    protected function error($code=0,$msg = '', $data = '', $url = null, $wait = 3, array $header = [])
    {
        if (is_string($code)) {
            $data = $msg;
            $msg = $code;
            $code = 0;
        }
        if (is_numeric($msg)) {
            $code = $msg;
            $msg  = '';
        }
        $result = [
            'code' => $code,
            'msg'  => $msg,
            'data' => $data
        ];
        $this->cross_iframe($result);
        $type = $this->getResponseType();
        if ('html' == strtolower($type)) {
            if (is_null($url)) {
                $url = 'javascript:history.back(-1);';
            } elseif ('' !== $url) {
                $url = (strpos($url, '://') || 0 === strpos($url, '/')) ? $url : Url::build($url);
            }
            $result['url'] = $url;
            $result['wait'] = $wait;
            $result = ViewTemplate::instance(Config::get('template'), Config::get('view_replace_str'))
                ->fetch(Config::get('dispatch_error_tmpl'), $result);
        }
        $response = Response::create($result, $type)->header($header);
        throw new HttpResponseException($response);
    }

    /**
     * 强制JSON返回  扩展
     * @access protected
     * @param mixed     $data 要返回的数据
     * @param integer   $code 返回的code
     * @param mixed     $msg 提示信息
     * @return void
     */
    protected function json($data, $code = 1, $msg = '')
    {
        $result = [
            'code' => $code,
            'msg'  => $msg,
            'data' => $data,
        ];
        $this->cross_iframe($result);
        $response = Response::create($result,'json')->header([]);
        throw new HttpResponseException($response);
    }
    /**
     * 跨域请求处理  扩展
     */
    protected function cross_domain($callback_name=''){
        $authdomain = cache('authdomain');
        if(!is_array($authdomain)){
            $authdomain = model('Authdomain')->cache();
        }
        $callback_name = ($callback_name?$callback_name:'callback');
        if(isset($_SERVER['HTTP_ORIGIN']) && !isset($_POST['submits'])){ //HTML5跨域请求
            $http_origin = parse_url($_SERVER['HTTP_ORIGIN']);
            $http_host = $http_origin['host'];
            if(in_array($http_host,$authdomain)){
                header('Access-Control-Allow-Origin: '.$http_origin['scheme'].'://'.$http_host);
                header('Access-Control-Allow-Credentials:true');
            }
            if($_SERVER['REQUEST_METHOD']=='OPTIONS'){
                header('Access-Control-Allow-Headers:request_type');
                $this->json(null,0,'ok');
            }
        }else if(isset($_SERVER['CONTENT_TYPE']) && stripos($_SERVER['CONTENT_TYPE'],'form')!==false){ //form-iframe跨域请求
            $this->is_cross_domain_iframe = true;
            $http_referer = parse_url($_SERVER['HTTP_REFERER']);
            $this->is_cross_domain_http_host = $http_referer['host'];
            $this->is_cross_domain_callback = $callback_name;
            if(!in_array($this->is_cross_domain_http_host,$authdomain)){
                $this->error('域未授权');
            }
        }
    }
    //扩展
    private function cross_iframe($result){
        if($this->is_cross_domain_iframe){
            $result = (is_array($result)?json_encode($result,JSON_UNESCAPED_UNICODE):$result);
            echo '<iframe src="//'.$this->is_cross_domain_http_host.'/cross_domain_proxy.html?callback='.$this->is_cross_domain_callback.'#'.urlencode($result).'"></iframe>';
            exit;
        }
    }
    /**
     * 返回封装后的API数据到客户端
     * @access protected
     * @param mixed     $data 要返回的数据
     * @param integer   $code 返回的code
     * @param mixed     $msg 提示信息
     * @param string    $type 返回数据格式
     * @param array     $header 发送的Header信息
     * @return void
     */
    protected function result($data, $code = 0, $msg = '', $type = '', array $header = [])
    {
        $result = [
            'code' => $code,
            'msg'  => $msg,
            'time' => $_SERVER['REQUEST_TIME'],
            'data' => $data,
        ];
        $this->cross_iframe($result);
        $type     = $type ?: $this->getResponseType();
        $response = Response::create($result, $type)->header($header);
        throw new HttpResponseException($response);
    }

    /**
     * URL重定向
     * @access protected
     * @param string         $url 跳转的URL表达式
     * @param array|integer  $params 其它URL参数
     * @param integer        $code http code
     * @return void
     */
    protected function redirect($url, $params = [], $code = 302)
    {
        $response = new Redirect($url);
        if (is_integer($params)) {
            $code   = $params;
            $params = [];
        }
        $response->code($code)->params($params);
        throw new HttpResponseException($response);
    }

    /**
     * 获取当前的response 输出类型
     * @access protected
     * @return string
     */
    protected function getResponseType()
    {
        $isAjax = Request::instance()->isAjax();
        return $isAjax ? Config::get('default_ajax_return') : Config::get('default_return_type');
    }
}
